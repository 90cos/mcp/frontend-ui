from brain.static import RPC
from brain import connect
from time import sleep


conn = connect()
# check if desired state is Active and PluginState is Stopped

serv_filter = {"DesiredState": "Stop", "ServiceName": "Plugin1-10_10_10_10-9999tcp"}
serv_filter2 = {"ServiceName": "Plugin1-10_10_10_10-9999tcp"}
serv_filter3 = {"DesiredState": "Activate", "ServiceName": "Plugin1-10_10_10_10-9999tcp"}
updated = {"State": "Stopped", "DesiredState": ""}
updated2 = {"State": "Active", "DesiredState": ""}
# success = RPC.filter(serv_filter).update(updated).run(conn)

plugin_list = RPC.filter(serv_filter).run(conn)
while True:
    for items in plugin_list:
        print(items)

    if plugin_list:
        print("TRUE")
        RPC.filter(serv_filter).update(updated).run(conn)
        RPC.filter(serv_filter3).update(updated2).run(conn)
        print("Done")
    sleep(2)
