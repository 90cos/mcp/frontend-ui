""" Docstrings """
import json
from django.shortcuts import render
from django.http import HttpResponse

from Backend.db_dir.custom_queries import get_log_data, download_log_data
from Backend.backend_help_func import cc_helper_function_one


def render_log_page(request):
    """
    Renders log page with logs data
    :return: log page in the ui
    """
    return render(request,
                  'logs_app/logs_page.html',
                  context={})  # query logs table here to render page with logs data


def log_data_controller(request):
    """
    :param request:
    :return:
    """
    return cc_helper_function_one(request, "GET", get_log_data)


def download_logs(request):
    if request.method == "GET":
        logs_type = request.GET.get("selected_download_logs")
        session_time = request.GET.get("session_time")
        response = download_log_data(logs_type, session_time)
    return HttpResponse(json.dumps(response),
                        content_type='application/json')
