from django.urls import path

from .views import get_index_data

app_name = 'index_app'
urlpatterns = [
    path('', get_index_data),  # default home page
    path('pcp_home/', get_index_data),  # default home page
    ]
