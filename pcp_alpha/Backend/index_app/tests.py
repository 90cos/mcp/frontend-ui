import pytest
from brain import connect
from pcp_alpha.Backend.db_dir.project_db import check_dev_env, rtdb
from .views import get_index_data


def test_target_list():
    """
    This test queries all the targets in
    Brain.Targets to be displayed in W1.
    """
    db_name = "Brain"
    db_table = "Targets"
    query_plugin_names = rtdb.db(db_name).table(db_table).pluck('PluginName', 'Location').run(connect())

    for plugin_item in query_plugin_names:
        assert isinstance(plugin_item, dict)


def test_home_page(admin_client):
    """
    This test checks if the web server displays
    the home page.
    :param admin_client:
    :return:
    """
    home_url = '/pcp_home/'
    response = admin_client.get(home_url)
    assert response.status_code == 200
