from django.urls import path
from django.conf import settings
from django.contrib.auth import views as auth_views

from .views import create_new_account, default_admin

app_name = 'registration_app'
urlpatterns = [
    path('', auth_views.LoginView, {'template_name': 'registration_app/login.html'}, name='login'),      # login page
    path('logout/', auth_views.LogoutView, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),  # logout
    path('action/create_account/', create_new_account),                                              # create account
    path('default_admin/', default_admin)                                                         # create default admin
    ]
