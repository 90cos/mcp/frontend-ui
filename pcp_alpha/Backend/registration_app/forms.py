"""
For future forms in the future
"""
from django.contrib.auth.forms import UserCreationForm, forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class CreateAccountForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("username", "email", "password",)

    def clean_username(self):
        username = self.clean_data["username"]
        # try:
        #     User.objects.get(username=username)
        # except ObjectDoesNotExist:
        #     raise username
        # raise forms.ValidationError(u'Username {} is already in use.'.format(username))

        if User.objects.exclude(pk=self.instance.pk).filter(username=username).exists():
            raise forms.ValidationError(u'Username {} is already in use.'.format(username))
        return username
