import os
import sys

from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

# from .forms import CreateAccountForm


def create_auth_user_table():
    """

    :return:
    """
    try:
        if User.objects.exists():
            print("Users table exists!")
        else:
            print("Users table DOESN'T exists, so pcp will be creating the Users table automatically.")
    except Exception as err_msg:
        print("\nException create_auth_user_table err_msg == {}".format(err_msg))
        print("migrations will be automated\n")
        cmd_migrate_auth = "python3 pcp_alpha/manage.py migrate auth"
        cmd_migrate = "python3 pcp_alpha/manage.py migrate"

        os.system(cmd_migrate_auth)
        os.system(cmd_migrate)


def create_user_type(username, password, user_role, active_bool):
    """

    :param username:
    :param password:
    :param user_role:
    :param active_bool:
    :return:
    """
    try:
        msg = True
        if user_role == "admin":
            User.objects.create_superuser(username=username, email='', password=password, is_active=active_bool)
        elif user_role == "user":
            User.objects.create_user(username=username, email='', password=password, is_active=active_bool)
        else:
            msg = False
    except Exception:
        msg = False
    return msg


def create_new_account(request):
    """

    :param request:
    :return:
    """
    # check if users table exists
    # create_auth_user_table()

    if request.method == "POST":

        # get username, password, and role type from user input
        username = request.POST.get("create_username")
        password = request.POST.get("create_password")
        user_role = request.POST.get("create_user_role")

        # creates user
        create_user_obj = create_user_type(username, password, user_role, False)
        if create_user_obj:
            # authenticate user
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("/pcp_home/")

    return redirect("/")


def default_admin(request):
    # create_auth_user_table()
    username = "ramrod"
    password = "ramrodpcp"
    user_role = "admin"
    if request.method == 'GET':
        print("\nCreating {} {} account".format(username, user_role))
        try:
            check_user = User.objects.get(username=username)
            print("{} already EXISTS.".format(check_user))
        except ObjectDoesNotExist:
            create_user_type(username, password, user_role, True)
            print("Done creating {} {} account\n".format(username, user_role))
    return redirect("/")
