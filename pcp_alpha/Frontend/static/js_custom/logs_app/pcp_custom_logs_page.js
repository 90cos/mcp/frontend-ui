/*
-----------------------------------------------------------------------------------------------------
This file was created for project pcp for logs specifically the logs page using javascript.
-----------------------------------------------------------------------------------------------------
*/

var increment_num = 0,
    log_datatable_id = 0,
    session_time = undefined;


var LOGS_DATA_TABLE = $('#logs_table').DataTable({
    searching: true,
    'createdRow': function( row, data, dataIndex ) {
        $(row).eq(0).attr('id', log_datatable_id);
        log_datatable_id++;
    },
});

function notification_log_function(msg1, msg2, param_type="info"){
    var notification_msg = msg1 + " " + msg2;
    $.notify({
        // options
        icon: 'glyphicon glyphicon-warning-sign',
        title: 'PCP Notification: ',
        message: notification_msg,
        // url: 'https://github.com/mouse0270/bootstrap-notify',
        target: '_blank'
    },{
        // settings
        element: 'body',
        position: null,
        type: param_type,
        allow_dismiss: true,
	    newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
	    },
        offset: 20,
        spacing: 10,
        z_index:1031,
        delay: 4000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated bounceInLeft',
            exit: 'animated bounceOutRight'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
}

function download_logs(){
    var selected_download_logs = $(this)[0].id;
    $.ajax({
        type: "GET",
        url: "/download_logs/",
        data: {"selected_download_logs": selected_download_logs,
               "session_time": session_time},
        datatype: 'json',
        success: function (data) {
            notification_log_function(data, "was uploaded to the brain.")
        },
    });
}

function update_logs_data_table(param_ts, param_sh, param_sc, param_ll, param_lm){
    if(param_sh === undefined){
        param_sh = "";
    }
    if(param_ll === undefined){
        param_ll = "";
    }
    LOGS_DATA_TABLE.row.add([date_time_test(param_ts), param_sh, param_sc, param_ll, param_lm]).draw( false );
    data_logs_limit(LOGS_DATA_TABLE);
}

function logs_change_ws_callback(message){
    var log_data_js = JSON.parse(message.data);
    if(session_time === undefined) {
        session_time = log_data_js.rt;
    }
    update_logs_data_table(
        log_data_js.rt,
        log_data_js.shost,
        log_data_js.sourceServiceName,
        log_data_js.Severity,
        log_data_js.msg
    );
    // update the right side panel
    var message_data = message.data;
    if(message_data.includes("{") && log_data_js.Severity >= 30){
        sidebar_log_list("danger", sidebar_log_prep(message_data));
    }
}

function data_logs_limit(log_data){
    if(log_data.data().length > 2000) {
        LOGS_DATA_TABLE.row("#"+increment_num).remove().draw();
        increment_num++;
    }
}

$(document).ready(function() {
    get_data_logs("logs");
    $("#download_all_id").click(download_logs);
    $("#download_session_id").click(download_logs);
} );
