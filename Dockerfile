FROM alpine:3.13

RUN apk update && \
    apk add --no-cache python3 python3-dev py3-pip libmagic supervisor nginx

RUN mkdir /run/nginx
RUN chown nginx /run/nginx

RUN pip3 install --extra-index-url https://MCP-Package-Repo:${PIP_ACCESS_TOKEN}@gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain
RUN pip3 install  --upgrade django==2.2.12 docker ua-parser

WORKDIR /srv/app

COPY . .

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /srv/app/pcp_alpha

EXPOSE 8080

#ENTRYPOINT [ "/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
#ENTRYPOINT python3 manage.py runserver 0.0.0.0:8080
CMD /srv/app/start_webserver.sh